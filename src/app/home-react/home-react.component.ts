import { Component, OnInit, Input, OnDestroy }             from '@angular/core';
import { Subscription, ReplaySubject }                     from 'rxjs';

 
import { RegistrationService }                             from '../../services/registration.service';
import { mainData, Book }                                   from '../../models/mainData';

import {PageEvent} from '@angular/material/paginator';

@Component({
selector:                 'app-home-react',
templateUrl:              './home-react.component.html',
styleUrls:               ['./home-react.component.css']
})

export class HomeReactComponent implements OnInit, OnDestroy {
    public                      queryText;
    subscription:               Subscription;
    main_data:                  mainData;
    //subject$:                   ReplaySubject<Book[]> = new ReplaySubject<Book[]>(1);
    results_num                 = 0;
    public length               = 10;
    public pageSize             = 20;
     public currentPage         = 1;
    public dataSource:           any;

    // MatPaginator Output
    pageEvent: PageEvent;
    public handlePage(event: any) {
        this.currentPage = event.pageIndex;
        this.pageSize    = event.pageSize;
        this.backend_api();
        return event;
    }

    

    constructor(   private _RegistrationService: RegistrationService ) {
        // subscribe to home component messages
        this.subscription = this._RegistrationService.getMessage().subscribe(data => {
            this.queryText = data.message;
            this.backend_api();
        });
    }

    

    ngOnInit() {
        if (this.queryText) {
            this.backend_api();
        }
    }

    private backend_api() {
        this._RegistrationService.getBooks(this.queryText, this.currentPage).subscribe(
            data => {
                this.results_num  = data['total-results'];
                this.main_data    = data;
                this.length       = parseInt(data['total-results']);
                if (data.results) {
                    this.dataSource   = data.results.work
                }

            },
            error => {

            },
            //() => console.log('good!')
        );
    }



    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }



 






}
