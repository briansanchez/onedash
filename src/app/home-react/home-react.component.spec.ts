import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeReactComponent } from './home-react.component';

describe('HomeReactComponent', () => {
  let component: HomeReactComponent;
  let fixture: ComponentFixture<HomeReactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeReactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeReactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
