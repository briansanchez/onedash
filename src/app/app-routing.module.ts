import { NgModule }                            from '@angular/core';
import { Routes, RouterModule }                from '@angular/router';

import { HomeReactComponent }                   from './home-react/home-react.component';
 

const routes: Routes = [
  { path: '',                                component: HomeReactComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
