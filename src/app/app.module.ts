import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { AppRoutingModule }                       from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import this new file
//import { CustomMaterialModule } from './material';

import { HomeReactComponent }                     from './home-react/home-react.component';

import {MAT_FORM_FIELD_DEFAULT_OPTIONS}            from '@angular/material/form-field';
import {DemoMaterialModule}                          from './material-module';

import {MatNativeDateModule} from '@angular/material/core';


@NgModule({
  declarations: [
    AppComponent,
    HomeReactComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'dotdash-goodreads' }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    MatNativeDateModule,

    //CustomMaterialModule

  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


