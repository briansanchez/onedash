import { Component, OnInit, OnDestroy, PLATFORM_ID, Inject }  from '@angular/core';
import { RegistrationService }                                from '../services/registration.service';
import { Subscription }                                       from 'rxjs/Subscription';
import { FormGroup,   Validators }                            from '@angular/forms';
import { FormBuilder }                                        from '@angular/forms';

@Component({
  selector:              'app',
  templateUrl:           './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy  {
    simpleForm:               FormGroup;

    private subscription: Subscription;

    constructor(    @Inject(PLATFORM_ID) private platform: Object,      private fb: FormBuilder,        private _RegistrationService: RegistrationService   ) {
    }
    title = 'ngkol';
    ngOnInit() {
        this.simpleForm = this.fb.group({
            queryText:                              ['',    [Validators.required]]
        });
    }
    get queryText() {
        return this.simpleForm.get('queryText');
    }

    onSearchChange() {  
        this._RegistrationService.sendMessage(this.queryText.value);
    }

    ngOnDestroy(){
        if ( this.subscription && this.subscription instanceof Subscription) {
            this.subscription.unsubscribe();
        }
    }
}
