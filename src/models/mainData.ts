
export class mainData {
    results_start:    number
    results_end:      number
    total_results:    number
    results:          Results
}

export class Results{
    work:        Work
}
export class Work {

    best_book:       Book;
}


export class Book {
    id:          number
    title:       string
    image_url:   string
    author:      Author
}


export class Author {
    name:            string;


}


