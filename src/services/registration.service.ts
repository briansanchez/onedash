import { Injectable }                 from '@angular/core';
import { HttpClient }                 from '@angular/common/http';
import { Observable, Subject }        from 'rxjs';
import { mainData }                   from '../models/mainData';
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
    //private baseUrl  = 'http://69.16.201.27:8000/api/'
    private baseUrl  = 'https://goodreads.square400.com/api/'
    private subject  = new Subject<any>();
    private subject2 = new Subject<any>();
    constructor(private http: HttpClient) { }


    sendMessage(queryText: string) {
        if (queryText) {
            this.subject.next({ message: queryText });
        }
    }
    getBooks(queryText: string, page: number): Observable<mainData>  {
        let apiDetailEndpoint = `${this.baseUrl}books/${queryText}/${page}/`
          return this.http.get<mainData>(apiDetailEndpoint );
    }

    getMessage(): Observable<any> {
      return this.subject.asObservable();
    }
}
